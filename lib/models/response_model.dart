class ResponseModel{
  final bool status;
  final String? msg;

  ResponseModel({required this.status, this.msg});
}