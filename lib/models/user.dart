class UserModel{
  int? id;
  String? email;
  String? userName;
  String? password;

  UserModel({this.id,this.email, this.userName, this.password});

  UserModel.fromJson(Map<String, dynamic> json)
      : id = json['id'],
        email = json['email'],
        password = json['password'],
        userName = json['name'];

  Map<String, dynamic> toJson() => {
    'name' : userName,
    'email': email,
    'password': password,
  };
}