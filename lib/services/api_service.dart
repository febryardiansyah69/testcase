import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;
import 'package:majootestcase/utils/error_helper.dart';

class ApiServices{

  Future<MovieModel> getMovieList() async {
    try {
      final _dio = await dioConfig.dio();
      Response response = await _dio.get('');
      final data = MovieModel.fromJson(response.data);
      return data;
    } on DioError catch(e) {
      print(e.message);
      throw ErrorHelper.extractApiError(e);
    }
  }
}