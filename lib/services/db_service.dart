import 'package:majootestcase/models/response_model.dart';
import 'package:majootestcase/utils/constant.dart';
import 'package:path/path.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:sqflite/sqflite.dart';

import '../models/user.dart';

class DbService{
  // initialize database
  Future<Database> initializeDb()async{
    // get default database location
    String path = await getDatabasesPath();
    // create database
    return openDatabase(
      join(path,'user.db'),
      onCreate: (database,version)async{
        // create table user
        await database.execute(
          "CREATE TABLE users(id INTEGER PRIMARY KEY AUTOINCREMENT, name TEXT NOT NULL, email TEXT NOT NULL,password TEXT NOT NULL)",
        );
      },
      version: 1,
    );
  }

  // create or register user
  Future<ResponseModel>createUser(UserModel user)async{
    try{
      // call database that we've initialized before
      Database db = await initializeDb();
      // selecting email user table
      final res = await db.rawQuery('SELECT * FROM users WHERE email=?', ['${user.email}',]);
      // check if it not empty
      if (res.isNotEmpty) {
        // return message email already registered
        return ResponseModel(status: false,msg: 'Email already registered');
      }
      await db.insert('users', user.toJson());
      return ResponseModel(status: true,msg: 'Register success');
    }on DatabaseException catch(e){
      return ResponseModel(status: false,msg: e.result.toString());
    }
  }

  Future<ResponseModel>loginUser(UserModel user)async{
    try{
      Database db = await initializeDb();
      final res = await db.rawQuery('SELECT * FROM users WHERE email=? and password=?', ['${user.email}', '${user.password}',]);
      if (res.isEmpty) {
        return ResponseModel(status: false,msg: 'Email or Password is not match');
      }
      // parsing data from local to dart object
      UserModel parsedUser = res.map((e) => UserModel.fromJson(e)).toList()[0];
      // save id to local
      _saveId(id: parsedUser.id!);
      return ResponseModel(status: true,msg: 'Login success');
    }on DatabaseException catch(e){
      return ResponseModel(status: false,msg: e.result.toString());
    }
  }

  void _saveId({required int id})async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.setInt(Preference.KEY_ID, id);
  }

  Future<bool> checkId()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    print('ID => ${prefs.getInt(Preference.KEY_ID)}');
    return prefs.getInt(Preference.KEY_ID) != null;
  }

  void removeId()async{
    SharedPreferences prefs = await SharedPreferences.getInstance();
    await prefs.remove(Preference.KEY_ID);
  }
}