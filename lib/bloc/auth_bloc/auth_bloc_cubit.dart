import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';
import '../../services/db_service.dart';
part 'auth_bloc_state.dart';

class AuthBlocCubit extends Cubit<AuthBlocState> {
  AuthBlocCubit() : super(AuthBlocInitialState());
  final _service = DbService();

  void fetchHistoryLogin() async{
    emit(AuthBlocInitialState());
    bool isLoggedIn = await _service.checkId();
    if(isLoggedIn){
      emit(AuthBlocLoggedInState(''));
    }else{
      emit(AuthBlocLoginState());
    }
  }

  void loginUser(UserModel user) async{
    emit(AuthBlocLoadingState());
    try{
      final res = await _service.loginUser(user);
      if (res.status) {
        emit(AuthBlocLoggedInState(res.msg!));
      } else {
        emit(AuthBlocErrorState(res.msg!));
      }
    }catch(e){
      print(e);
      emit(AuthBlocErrorState(e.toString()));
    }
  }
}
