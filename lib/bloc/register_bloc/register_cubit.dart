import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/user.dart';

import '../../services/db_service.dart';

part 'register_state.dart';

class RegisterCubit extends Cubit<RegisterState> {
  RegisterCubit() : super(RegisterInitialState());
  final _service = DbService();

  void register({required UserModel user})async{
    emit(RegisterLoadingState());
    try{
      final res = await _service.createUser(user);
      if (res.status) {
        emit(RegisterSuccessState(res.msg ?? ''));
      } else {
        emit(RegisterFailureState(res.msg ?? ''));
      }
    }catch(e){
      print(e);
      emit(RegisterFailureState(e.toString()));
    }
  }
}
