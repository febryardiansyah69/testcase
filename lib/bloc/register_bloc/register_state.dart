part of 'register_cubit.dart';

abstract class RegisterState extends Equatable {
  const RegisterState();
  @override
  List<Object> get props => [];
}

class RegisterInitialState extends RegisterState {}
class RegisterLoadingState extends RegisterState {}
class RegisterSuccessState extends RegisterState {
  final String msg;

  RegisterSuccessState(this.msg);

  @override
  List<Object> get props => [msg];
}
class RegisterFailureState extends RegisterState {
  final String msg;

  RegisterFailureState(this.msg);

  @override
  List<Object> get props => [msg];
}
