import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/detail/detail_page.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
   final List<MovieResults> data;

  const HomeBlocLoadedScreen({Key? key,required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: ListView.builder(
      itemCount: data.length,
      itemBuilder: (context, index) {
        return movieItemWidget(data[index],context);
      },
    ),
    );
  }

  Widget movieItemWidget(MovieResults data,BuildContext context){
    return GestureDetector(
      onTap: (){
        Navigator.push(context, MaterialPageRoute(builder: (_)=>DetailPage(data: data)));
      },
      child: Card(
        shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.all(Radius.circular(25.0)
            )
        ),
        child: Column(
          children: [
            Padding(
              padding: EdgeInsets.all(25),
              child: Image.network("https://image.tmdb.org/t/p/w500/${data.posterPath}",),
            ),
            Padding(
              padding: EdgeInsets.symmetric(vertical: 8, horizontal: 24),
              child: Text(data.name ?? data.title ?? '', textDirection: TextDirection.ltr,style: TextStyle(
                fontWeight: FontWeight.bold,fontSize: 16,
              )),
            )
          ],
        ),
      ),
    );
  }
}
