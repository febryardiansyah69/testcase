import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';

class DetailPage extends StatelessWidget {
  final MovieResults data;

  const DetailPage({Key? key,required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: SingleChildScrollView(
          child: Column(
            children: [
              Stack(
                children: [
                  Container(
                    width: MediaQuery.of(context).size.width,
                    height: MediaQuery.of(context).size.height * 0.4,
                  ),
                  ShaderMask(
                    shaderCallback: (rect){
                      return LinearGradient(
                        begin: Alignment.topCenter,
                        end: Alignment.bottomCenter,
                        colors: [Colors.black, Colors.transparent],
                      ).createShader(Rect.fromLTRB(0, 0, rect.width, rect.height));
                    },
                    blendMode: BlendMode.dstIn,
                    child: Container(
                      width: MediaQuery.of(context).size.width,
                      height: MediaQuery.of(context).size.height * 0.4,
                      decoration: BoxDecoration(
                        image: DecorationImage(
                          image: NetworkImage('https://image.tmdb.org/t/p/w500/${data.posterPath}'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                  Positioned(
                    top: 10,
                    left: 10,
                    child: GestureDetector(
                      onTap: ()=>Navigator.pop(context),
                      child: CircleAvatar(
                        backgroundColor: Colors.black.withOpacity(0.6),
                        child: Icon(Icons.arrow_back,color: Colors.white,),
                      ),
                    ),
                  ),
                  Align(
                    alignment: Alignment.bottomCenter,
                    child: Container(
                      margin: EdgeInsets.only(top: MediaQuery.of(context).size.height * 0.15),
                      width: 100,
                      height: 140,
                      decoration: BoxDecoration(
                        color: Colors.grey,
                        image: DecorationImage(
                          image: NetworkImage('https://image.tmdb.org/t/p/w500/${data.posterPath}'),
                          fit: BoxFit.cover,
                        ),
                      ),
                    ),
                  ),
                ],
              ),
              Text(data.title ?? data.name ?? '',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 17),),
              Padding(
                padding: EdgeInsets.all(8),
                child: Table(
                  children: [
                    TableRow(
                        children: [
                          TableCell(child: Text('Tahun rilis'),),
                          TableCell(child: Text(': ${data.releaseDate ?? ''}'),),
                        ]
                    ),
                    TableRow(
                        children: [
                          TableCell(child: Text('Rating'),),
                          TableCell(child: Text(': ${data.voteAverage}'),),
                        ]
                    ),
                    TableRow(
                        children: [
                          TableCell(child: Text('Popularity'),),
                          TableCell(child: Text(': ${data.popularity}'),),
                        ]
                    ),
                  ],
                ),
              ),
              Padding(
                padding: EdgeInsets.all(8),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text('Overview',style: TextStyle(fontWeight: FontWeight.bold,fontSize: 16),),
                    SizedBox(height: 8,),
                    Text(data.overview ?? ''),
                  ],
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
