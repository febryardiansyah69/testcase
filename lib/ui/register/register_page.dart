import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:majootestcase/bloc/register_bloc/register_cubit.dart';
import 'package:majootestcase/models/user.dart';
import 'package:majootestcase/ui/extra/loading_dialog.dart';

import '../../common/widget/custom_button.dart';
import '../../common/widget/text_form_field.dart';

class RegisterPage extends StatefulWidget {
  const RegisterPage({Key? key}) : super(key: key);

  @override
  _RegisterPageState createState() => _RegisterPageState();
}

class _RegisterPageState extends State<RegisterPage> {
  final _nameController = TextController();
  final _emailController = TextController();
  final _passwordController = TextController();
  GlobalKey<FormState> formKey = new GlobalKey<FormState>();

  bool _isObscurePassword = true;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        backgroundColor: Colors.white,
        elevation: 0,
        leading: BackButton(
          onPressed: ()=>Navigator.pop(context),
          color: Colors.black,
        ),
      ),
      body: BlocListener<RegisterCubit, RegisterState>(
        listener: (context, state) {
          if (state is RegisterLoadingState) {
            showMyLoadingDialog(context, true);
          }
          if (state is RegisterFailureState) {
            Navigator.pop(context);
            showMyLoadingDialog(context, false,msg: state.msg);
          }
          if (state is RegisterSuccessState) {
            Navigator.pop(context);
            showMyLoadingDialog(context, false,msg: state.msg);
            setState(() {
              _nameController.textController.clear();
              _emailController.textController.clear();
              _passwordController.textController.clear();
            });
          }
        },
        child: SingleChildScrollView(
          child: Padding(
            padding: EdgeInsets.only(top: 75, left: 25, bottom: 25, right: 25),
            child: Column(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  'Daftar',
                  style: TextStyle(
                    fontSize: 24,
                    fontWeight: FontWeight.bold,
                    // color: colorBlue,
                  ),
                ),
                Text(
                  'Silahkan buat akun baru terlebih dahulu',
                  style: TextStyle(
                    fontSize: 15,
                    fontWeight: FontWeight.w400,
                  ),
                ),
                SizedBox(
                  height: 9,
                ),
                _form(),
                SizedBox(
                  height: 50,
                ),
                CustomButton(
                  text: 'Register',
                  onPressed: handleRegister,
                  height: 100,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _form() {
    return Form(
      key: formKey,
      child: Column(
        children: [
          CustomTextFormField(
            context: context,
            controller: _nameController,
            hint: 'John Doe',
            label: 'Name',
          ),
          CustomTextFormField(
            context: context,
            controller: _emailController,
            isEmail: true,
            hint: 'Example@123.com',
            label: 'Email',
            validator: (val) {
              final pattern = new RegExp(r'([\d\w]{1,}@[\w\d]{1,}\.[\w]{1,})');
              if (val != null)
                return pattern.hasMatch(val) ? null : 'email is invalid';
              return null;
            },
          ),
          CustomTextFormField(
            context: context,
            label: 'Password',
            hint: 'password',
            controller: _passwordController,
            isObscureText: _isObscurePassword,
            validator: (val){
              if (val != null) {
                return val.length < 6?'Password less than 6':null;
              }
              return null;
            },
            suffixIcon: IconButton(
              icon: Icon(_isObscurePassword ? Icons.visibility_off_outlined : Icons.visibility_outlined,
              ),
              onPressed: () {
                setState(() {
                  _isObscurePassword = !_isObscurePassword;
                });
              },
            ),
          ),
        ],
      ),
    );
  }

  void handleRegister() async {
    final _name = _nameController.value;
    final _email = _emailController.value;
    final _password = _passwordController.value;
    if (formKey.currentState!.validate() && _name.isNotEmpty && _email.isNotEmpty && _password.isNotEmpty ) {
      context.read<RegisterCubit>().register(user: UserModel(
        userName: _name,email: _email,password: _password,
      ));
    }
  }
}
