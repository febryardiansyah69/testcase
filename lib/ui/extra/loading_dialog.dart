import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

void showMyLoadingDialog(BuildContext context,bool isLoading,{String? msg}){
  showDialog(context: context, builder: (context)=>AlertDialog(
    title: !isLoading?Text('Info'):null,
    content: isLoading?Row(
      children: [
        CupertinoActivityIndicator(),
        SizedBox(width: 4,),
        Text('Loading..'),
      ],
    ):Text(msg ?? ''),
    actions: [
      if (!isLoading)
        ElevatedButton(onPressed: (){
          Navigator.pop(context);
        }, child: Text('Done')),
    ],
  ));
}
